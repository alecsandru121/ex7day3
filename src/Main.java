import java.lang.reflect.Array;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        String[] receptionisti = new String[5];
        int[] etaje = new int[5];
        receptionisti[0] = "John Snow";
        receptionisti[1] = "William Cooper";
        receptionisti[2] = "Patricia Johnson";
        receptionisti[3] = "Kate Anderson";
        receptionisti[4] = "Dustin Rhodes";

        etaje[0] = 1;
        etaje[1] = 2;
        etaje[2] = 3;
        etaje[3] = 4;
        etaje[4] = 5;

        System.out.println("The receptionist on the " + etaje[0] + "-st " + "is: " + receptionisti[0]);
        System.out.println("The receptionist on the " + etaje[1] + "-nd " + "is: " + receptionisti[1]);
        System.out.println("The receptionist on the " + etaje[2] + "-rd " + "is: " + receptionisti[2]);
        System.out.println("The receptionist on the " + etaje[3] + "-th " + "is: " + receptionisti[3]);
        System.out.println("The receptionist on the " + etaje[4] + "-th " + "is: " + receptionisti[4]);

        String[][] owner = new String[2][2];
        owner[0][0] = "Mr.";
        owner[0][1] = "Smith Riley";
        owner[1][0] = "Mrs.";
        owner[1][1] = "Catherine Jade";

        System.out.println(owner[0][0] + owner[0][1] + "and " + owner[1][0] + owner[1][1] + "are the owners of the Plaza Hotel.");

        char [] copyFrom = {'B', 'E', 'D', 'O', 'N', 'A', 'L', 'D', 'A', 'T', 'E'};
        char [] copyTo = new char [6];

        System.arraycopy(copyFrom, 2, copyTo, 0 , 6);
        System.out.println("The hotel's security is provided by: " + new String(copyTo)); // declarare + concatenare mesaj
    }
}
